//Raemond Bergstrom-Wood
//main.cpp
#include "snake.h";

int main()
{
	getHighScore();
	changeDifficulty();
	startUp();
	background();
	displayScore();
	Boundaries();
	int count = 0;
	while (run == true)
	{
		if (needFood == true)
		{
			getFoodCoord();
			needFood = false;
		}
		displayFood();
		snake();
		if ((count % difficulty) == 0)
			snakeMove();
		interaction();
		if (wallCollision() == true || snakeCollision() == true)
		{
			break;
		}
		if (foodCollision() == true)
		{
			increaseLength();
			getFoodCoord();
			score++;
			displayScore();
		}
		Sleep(50);//Inherently limits the frame rate. If I want to make the game even more difficult I can decrease the sleep time.
		count ++;
	}
	changeHighScore();
	if (newHighScore == true)
		newHigh();
	else
		gameOver();
	return 0;
}
//Raemond Bergstrom-Wood
//snake.h

#pragma once
#include <iostream>
#include <Windows.h>
#include <ctime>
#include <fstream>
#include <cstdlib>
using namespace std;

//Constants and global variables
const int SCREEN_WIDTH = 79;
const int SCREEN_HEIGHT = 40;
const int playerVelocity = 1;
HANDLE hOut;
RECT r;
int snakeLength = 3;
int snakeDirection = 4;//1 up, 2 is right, 3 is down, 4 is left
COORD snakePos;
COORD food;
int snakeX[300] = {};
int snakeY[300] = {};
int tempX = 1;
int tempY = 1;
int foodX = 1;
int foodY = 1;
int difficulty = 0;
bool needFood = true;
bool run = true;
int score = 0;
int highScore = 0;
bool newHighScore = false;

//Function Prototype
void increaseLength();
void displayScore();

//Set up game
void startUp()
{
	srand(static_cast<int>(time(0)));

	//Set the console window size
	HWND console = GetConsoleWindow();
	CONSOLE_SCREEN_BUFFER_INFO SBInfo;
	RECT r;
	GetWindowRect(console, &r);

	GetConsoleScreenBufferInfo(hOut, &SBInfo);
	SBInfo.dwSize.X = 2000;
	SBInfo.dwSize.Y = 600;
	MoveWindow(console, r.left, r.top, SBInfo.dwSize.X, SBInfo.dwSize.Y, TRUE);//width, height

	hOut = GetStdHandle( STD_OUTPUT_HANDLE );

	//Hide cursor
	CONSOLE_CURSOR_INFO ConCurInf;
	ConCurInf.dwSize = 10;
	ConCurInf.bVisible = false;
	SetConsoleCursorInfo(hOut, &ConCurInf);
	//end hide cursor

	//Start the snake at coords
	snakeX[0] = 30;
	snakeY[0] = 20;
	snakeX[1] = 31;
	snakeY[1] = 20;
	snakeX[2] = 32;
	snakeY[2] = 20;
}

//Make background Green
void background()
{
	SetConsoleTextAttribute( hOut, BACKGROUND_GREEN);
	//Rows
	for (int y = 0; y < 40; y++)
	{
		for (int x = 0; x < 79; x++)
			cout << " ";
		cout << endl;
	}
}
//Draw Boundaries
void Boundaries()
{
	COORD b;
	b.X = 0;
	b.Y = 0;
	SetConsoleCursorPosition( hOut, b);
	SetConsoleTextAttribute( hOut, BACKGROUND_RED );
	for (int count = 0; count < SCREEN_WIDTH; count ++)
		cout << " ";//"-";
	cout << endl;
	for (int count = 0; count < (SCREEN_HEIGHT -2); count ++)
		cout << " " << endl;//"|" << endl;
	for (int count = 0; count < SCREEN_WIDTH; count ++)
		cout << " ";//"-";
	b.X = 78;
	b.Y = 1;
	for (int count = 0; count < (SCREEN_HEIGHT -2); count ++)
	{
		SetConsoleCursorPosition( hOut, b);
		cout << " ";//"|";
		b.Y ++;
	}
}

//Draw Snake
void snake()
{
	for (int count = 0; count < snakeLength; count ++)
	{
		snakePos.X = snakeX[count];
		snakePos.Y = snakeY[count];
		SetConsoleCursorPosition( hOut, snakePos);
		SetConsoleTextAttribute( hOut, FOREGROUND_BLUE | BACKGROUND_RED);
		cout << "#";
	}

	//the temp will clear the space after the snake moves
	snakePos.X = tempX;
	snakePos.Y = tempY;
	SetConsoleCursorPosition( hOut, snakePos);
	SetConsoleTextAttribute( hOut, BACKGROUND_GREEN);
	cout << " ";
}

//Move Snake
void snakeMove()
{
	tempX = snakeX[snakeLength-1];
	tempY = snakeY[snakeLength-1];
	for (int count = snakeLength; count > 0; count--)
		{
			snakeX[count] = snakeX[count - 1];
			snakeY[count] = snakeY[count - 1];
		}
	if (snakeDirection == 1)
		snakeY[0] --;
	else if (snakeDirection == 2)
		snakeX[0] ++;
	else if (snakeDirection == 3)
		snakeY[0] ++;
	else if (snakeDirection == 4)
		snakeX[0] --;
}

//Check for button press
void interaction()
{
	if (GetAsyncKeyState( VK_UP ) && snakeDirection != 3)
		snakeDirection = 1;
	if (GetAsyncKeyState( VK_RIGHT ) && snakeDirection != 4)
		snakeDirection = 2;
	if (GetAsyncKeyState( VK_DOWN ) && snakeDirection != 1)
		snakeDirection = 3;
	if (GetAsyncKeyState( VK_LEFT ) && snakeDirection != 2)
		snakeDirection = 4;
	if (GetAsyncKeyState( VK_SPACE))
	{
		system("pause");
		system("cls");
		background();
		displayScore();
		Boundaries();
	}
	if (GetAsyncKeyState( VK_ESCAPE ))
		run = false;
}

//check for collision against the walls
bool wallCollision()
{
	if ((snakeX[0] <= 0) || (snakeX[0] >= 78))
		return true;
	else if ((snakeY[0] <= 0)|| (snakeY[0] >= 39))
		return true;
	else
		return false;
}

//Add 1 length
void increaseLength()
{
	if (snakeX[snakeLength-1]-snakeX[snakeLength-2]== 1)
	{
		snakeX[snakeLength] = snakeX[snakeLength-1]+1;
		snakeY[snakeLength] = snakeY[snakeLength-1];
	}
	else if (snakeX[snakeLength-1]-snakeX[snakeLength-2]== -1)
	{
		snakeX[snakeLength] = snakeX[snakeLength-1]-1;
		snakeY[snakeLength] = snakeY[snakeLength-1];
	}
	else if (snakeY[snakeLength-1]-snakeY[snakeLength-2]== 1)
	{
		snakeX[snakeLength] = snakeX[snakeLength-1];
		snakeY[snakeLength] = snakeY[snakeLength-1]+1;
	}
	else if (snakeY[snakeLength-1]-snakeY[snakeLength-2]== -1)
	{
		snakeX[snakeLength] = snakeX[snakeLength-1];
		snakeY[snakeLength] = snakeY[snakeLength-1]-1;
	}
	snakeLength ++;
}

//Create the food particles
void getFoodCoord()
{
	int lb = 1;
	int xub = 77;
	int yub = 38;
	foodX = lb + rand()%(xub - lb + 1);
	foodY = lb + rand()%(yub - lb + 1);
	food.X = foodX;
	food.Y = foodY;
}

//Display the food
void displayFood()
{
	SetConsoleCursorPosition( hOut, food);
	cout << "*";
}

//check for the collision between the worm and the food
bool foodCollision()
{
	if ((snakeX[0] == food.X) && (snakeY[0] == food.Y))
		return true;
	else
		return false;
}

//check for snake's collision with itself
bool snakeCollision()
{
	bool collision = false;
	for (int count = 1; count < snakeLength; count ++)
	{
		if (snakeX[0] == snakeX[count] && snakeY[0] == snakeY[count])
			collision = true;
	}
	return collision;
}

//Display Score
void displayScore()
{
	COORD s;
	s.X = 19;
	s.Y = 40;
	SetConsoleCursorPosition( hOut, s);
	cout<< "Your Score:\t" << score <<"\tHigh Score:\t" << highScore;
}

//Get high score
void getHighScore()
{
	ifstream m;
	m.open("highScore.txt");
	if(m == NULL)//If the "highScore.txt" does not exist, it will create one and set the high score to 0
	{
		cout << "File error opening 'highScores.txt.'" << endl;
		cout << "Let me create one for you." << endl;
		m.close();
		ofstream h;
		h.open ("highScore.txt");
		h << "0";
		h.close ();
		system("pause");
		ifstream s;
		s.open("highScore.txt");
		while (!s.eof())
		{
			s >> highScore;
		}
		s.close();
		system("cls");
	}
	else
	{
		while (!m.eof())
		{
			m >> highScore;
		}
		m.close();
	}
}

//Change to new High Score if needed
void changeHighScore()
{
	if (score > highScore)
	{
		ofstream h;
		h.open ("highScore.txt");
		h << score;
		h.close ();
		newHighScore = true;
	}
}

//Ask user to enter either 1 or 2 for difficulty 1 being hard and 2 being easy
int changeDifficulty()
{
	SetConsoleTextAttribute( hOut, FOREGROUND_RED );
	cout << endl;
	cout << "    ad88888ba   888b      88         db         88      a8P   88888888888  " << endl;
	cout << "   d8'     '8b  8888b     88        d88b        88    ,88'    88" << endl;
	cout << "   Y8,          88 `8b    88       d8'`8b       88  ,88'      88" << endl;
	cout << "   `Y8aaaaa,    88  `8b   88      d8'  `8b      88,d88'       88aaaaa" << endl;
	cout << "     `'''''8b,  88   `8b  88     d8YaaaaY8b     8888'88,      88'''''" << endl;
	cout << "           `8b  88    `8b 88    d8''''''''8b    88P   Y8b     88" << endl;
	cout << "   Y8a     a8P  88     `8888   d8'        `8b   88     '88,   88" << endl;
	cout << "    'Y88888P'   88      `888  d8'          `8b  88       Y8b  88888888888" << endl << endl;;
	cout << "   You are about to play SNAKE by Raemond." << endl;
	cout << "   In order to play SNAKE you will need to manuver a snake around the board." << endl;
	cout << "   If you hit the wall or try to eat a part of your body, you will lose." << endl;
	cout << "   Try to beat the high score! Good Luck!" << endl << endl;
	cout << "   DIfficulty level selection" << endl;
	cout << "   1 is hard and 2 is easy" << endl;
	cout << "   Please enter either 1 or 2:\t";
	cin >> difficulty;
	while (difficulty != 1 && difficulty != 2)
	{
		cout << "Invalid Entry" << endl;
		cout << "1 is hard and 2 is easy" << endl;
		cout << "Please enter either 1 or 2:";
		cin >> difficulty;
	}
	system("cls");
	return difficulty;
}

//New High Score
void newHigh()
{
	SetConsoleTextAttribute( hOut, FOREGROUND_RED );
	system("cls");
	COORD r;
	r.X = 0;
	r.Y = 0;
	SetConsoleCursorPosition( hOut, r);
	while (!GetAsyncKeyState(VK_SPACE))
	{
		SetConsoleTextAttribute( hOut, FOREGROUND_RED );
		cout << endl << endl << endl;
		cout << "               88        88  88    ,ad8888ba,   88        88" << endl;
		cout << "               88        88  88   d8''    `'8b  88        88" << endl;
		cout << "               88        88  88  d8'            88        88" << endl;
		cout << "               88aaaaaaaa88  88  88             88aaaaaaaa88" << endl;
		cout << "               88''''''''88  88  88      88888  88''''''''88" << endl;
		cout << "               88        88  88  Y8,        88  88        88" << endl;
		cout << "               88        88  88   Y8a.    .a88  88        88" << endl;
		cout << "               88        88  88    `'Y88888P'   88        88" << endl << endl << endl;
		cout << "      ad88888ba     ,ad8888ba,    ,ad8888ba,    88888888ba   88888888888" << endl;
		cout << "     d8'     '8b   d8''    `'8b  d8''    `'8b   88      '8b  88" << endl;
		cout << "     Y8,          d8'           d8'        `8b  88      ,8P  88" << endl;
		cout << "     `Y8aaaaa,    88            88          88  88aaaaaa8P'  88aaaaa" << endl;
		cout << "       `'''''8b,  88            88          88  88''''88'    88'''''" << endl;
		cout << "             `8b  Y8,           Y8,        ,8P  88    `8b    88" << endl;
		cout << "     Y8a     a8P   Y8a.    .a8P  Y8a.    .a8P   88     `8b   88" << endl;
		cout << "      'Y88888P'     `'Y8888Y''    `'Y8888Y''    88      `8b  88888888888" << endl;
	}
}

void gameOver()
{
	SetConsoleTextAttribute( hOut, FOREGROUND_RED );
	system("cls");
	while (!GetAsyncKeyState(VK_SPACE))
	{
		COORD r;
		r.X = 0;
		r.Y = 0;
		SetConsoleCursorPosition( hOut, r);
		cout << endl << endl << endl;
		cout << "       ,ad8888ba,         db         88b           d88  88888888888" << endl;
		cout << "      d8''    `'8b       d88b        888b         d888  88" << endl;
		cout << "     d8'                d8'`8b       88`8b       d8'88  88" << endl;
		cout << "     88                d8'  `8b      88 `8b     d8' 88  88aaaaa" << endl;
		cout << "     88      88888    d8YaaaaY8b     88  `8b   d8'  88  88'''''" << endl;
		cout << "     Y8,        88   d8''''''''8b    88   `8b d8'   88  88" << endl;
		cout << "      Y8a.    .a88  d8'        `8b   88    `888'    88  88" << endl;
		cout << "       `'Y88888P'  d8'          `8b  88     `8'     88  88888888888" << endl << endl << endl;
		cout << "         ,ad8888ba,   8b           d8  88888888888  88888888ba" << endl;
		cout << "        d8''    `'8b  `8b         d8'  88           88      '8b" << endl;
		cout << "       d8'        `8b  `8b       d8'   88           88      ,8P" << endl;
		cout << "       88          88   `8b     d8'    88aaaaa      88aaaaaa8P'" << endl;
		cout << "       88          88    `8b   d8'     88'''''      88''''88'" << endl;
		cout << "       Y8,        ,8P     `8b d8'      88           88    `8b" << endl;
		cout << "        Y8a.    .a8P       `888'       88           88     `8b" << endl;
		cout << "         `'Y8888Y''         `8'        88888888888  88      `8b" << endl;
		cout << endl;
		Sleep(50);
	}
}